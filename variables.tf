# @group "Basic"
variable "gitlab_image" {
  type        = string
  description = "gitlab image"
  default     = "gitlab/gitlab-ce"
}

# @group "Basic"
variable "gitlab_root_password" {
  type        = string
  description = "gitlab root password"
  default     = "seal123456"
  sensitive   = true
}

# @group "AWS"
# @options ["t3.medium", "c5.xlarge"]
variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t3.medium"
}

# @group "AWS"
variable "disk_size" {
  type        = number
  description = "Root disk size in GiB"
  default     = 50
}

# @group "AWS"
variable "key_name" {
  type        = string
  description = "AWS key name"
  default     = "xueying"
}

# @group "AWS"
variable "security_group_name" {
  type        = string
  description = "Security group Name"
  default     = "all-open"
}

# @hidden
variable "gitlab_metadata_application_instance_name" {
  type        = string
  description = "gitlab metadata application instance name."
  default     = "bar"
}