output "gitlab_url" {
  description = "The URL of the GitLab instance"
  value = "http://${aws_instance.gitlab.public_ip}"
}