# Gitlab on AWS

This is a terraform module that will create a Gitlab instance on AWS.

## Providers

| Name | Version |
|------|---------|
| aws  | n/a     |

## Inputs

| Name                                      | Description                                |   Type   |       Default        | Required |
|-------------------------------------------|--------------------------------------------|:--------:|:--------------------:|:--------:|
| gitlab_image                              | Gitlab image                               | `string` | `"gitlab/gitlab-ce"` |    no    |
| gitlab_root_password                      | Gitlab root password                       | `string` |    `"seal123456"`    |    no    |
| instance\_type                            | Instance type                              | `string` |    `"t3.medium"`     |    no    |
| disk\_size                                | Root disk size in GiB                      | `number` |         `50`         |    no    |
| security\_group\_name                     | Security group Name                        | `string` |     `"all-open"`     |    no    |
| gitlab_metadata_application_instance_name | gitlab metadata application instance name. | `string` |       `"bar"`        |    no    |

## Outputs

| Name       | Description |
|------------|-------------|
| gitlab_url | Gitlab URL  |