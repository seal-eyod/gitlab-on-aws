data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


data "aws_security_group" "selected" {
  name = var.security_group_name
}

data "aws_subnets" "selected" {
  filter {
    name   = "vpc-id"
    values = [data.aws_security_group.selected.vpc_id]
  }
}

resource "aws_instance" "gitlab" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = data.aws_subnets.selected.ids.0
  vpc_security_group_ids = [data.aws_security_group.selected.id]
  key_name      = var.key_name
  user_data     = <<-EOF
                  #!/bin/bash
                  set -ex;
                  public_ip=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
                  curl -fsSL https://get.docker.com | bash && sudo usermod -aG docker ubuntu
                  docker run -d --privileged --restart=always -p 80:80 -p 443:443 \
                  -e GITLAB_ROOT_PASSWORD="${var.gitlab_root_password}" \
                  "${var.gitlab_image}"
                  EOF
  tags = {
    "Name" = "${var.gitlab_metadata_application_instance_name}-gitlab"
  }

  root_block_device {
    volume_size = var.disk_size
  }
}

resource "null_resource" "gitlab_health_check" {
  depends_on = [
    aws_instance.gitlab,
  ]

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    command     = "for i in `seq 1 100`; do curl -k -s $ENDPOINT >/dev/null && exit 0 || true; sleep 5; done; echo TIMEOUT && exit 1"
    interpreter = ["/bin/sh", "-c"]
    environment = {
      ENDPOINT = "http://${aws_instance.gitlab.public_ip}"
    }
  }
}
